import testinfra


def test_mariadb_audit_plugin_is_installed(File):
    assert File('/usr/lib/mysql/plugin/server_audit.so').is_file

def test_mcafee_audit_plugin_is_installed(File):
    assert File('/usr/lib/mysql/plugin/libaudit_plugin.so').is_file

def test_percona_mysqld_conf_is_present(File):
    assert File('/etc/mysql/percona-server.conf.d/mysqld.cnf').is_file

def test_percona_server_package_in_installed(Package):
    percona = Package('percona-server-server-5.7')
    assert percona.is_installed
    assert percona.version.startswith('5.7')

def test_service_is_running_and_enabled(Service):
    percona = Service('mysql')
    assert percona.is_running
    assert percona.is_enabled
