{% from "percona/map.jinja" import percona_settings with context %}
{% set os_family = salt['grains.get']('os_family') %}
{% set initsystem = salt['grains.get']('init') %}

include:
  - .deps

{% for name, user in percona_settings.db_users.items() %}
{%- if 'state' not in user.keys() %}
{%- do user.update({'state': 'present'}) %}
{%- endif %}
mysql_user_{{ name }}_{{ user['host'] }}:
# USER PRESENT
{% if user['state'] == "present" %}
  mysql_user.present:
  {%- if 'username' not in user.keys() %}
    - name: {{ name }}
  {%- else %}
    - name: {{ user.get('username', name) }}
  {%- endif %}
    - host: {{ user['host'] }}
  {%- if user['password_hash'] is defined %}
    - password_hash: '{{ user['password_hash'] }}'
  {%- elif user['password'] is defined and user['password'] != None %}
    - password: '{{ user['password'] }}'
  {%- endif %}
    - connection_host: localhost
    - connection_pass: {{ percona_settings.get('root_password', '') }}
    - require:
      - pkg: mysql_python_dep
      - service: mysql_service_running
{%   if os_family in ['RedHat', 'Suse'] %}
      - mysql_user: mysql_root_password
{%   endif %}
{% endif %}
# USER ABSENT
{% if user['state'] == "absent" %}
  mysql_user.absent:
  {%- if 'username' not in user.keys() %}
    - name: {{ name }}
  {%- else %}
    - name: {{ user.get('username', name) }}
  {%- endif %}
    - connection_host: localhost
    - connection_pass: {{ percona_settings.get('root_password', '') }}
    - require:
      - pkg: mysql_python_dep
      - service: mysql_service_running
{%  if os_family in ['RedHat', 'Suse'] %}
    - mysql_user: mysql_root_password
{%  endif %}
{% endif %}

# GRANTS PRESENT
{%   for db in user['database_grants'] %}
mysql_grant_{{ name }}_{{ user['host'] }}_{{ loop.index0 }}:
{% if user['state'] == "present" %}
  mysql_grants.present:
    - grant: '{{db['grant']|join(",")}}'
    - database: '{{ db['database'] }}'
  {%- if 'username' not in user.keys() %}
    - user: {{ name }}
  {%- else %}
    - user: {{ user.get('username', name) }}
  {%- endif %}
    - host: {{ user['host'] }}
    # TODO: Fix this with a proper multiple hosts management approach.
    - connection_host: localhost
    - connection_pass: {{ percona_settings.get('root_password', '') }}
    - grant_option: {{ db['grant_option']|default(False) }}
    - require:
      - pkg: mysql_python_dep
      - service: mysql_service_running
{% endif %}
# GRANTS ABSENT
{% if user['state'] == "absent" %}
  mysql_grants.absent:
    - grant: '{{db['grant']|join(",")}}'
    - database: '{{ db['database'] }}'
  {%- if 'username' not in user.keys() %}
    - user: {{ name }}
  {%- else %}
    - user: {{ user.get('username', name) }}
  {%- endif %}
    - host: {{ user['host'] }}
    - connection_host: localhost
    - connection_pass: {{ percona_settings.get('root_password', '') }}
    - require:
      - pkg: mysql_python_dep
      - service: mysql_service_running
{% endif %}
{%   endfor %}
{% endfor %}

{%- if percona_settings.db_name is defined %}
{% for name, database in percona_settings.db_name.items() %}
{%- if 'state' not in database.keys() %}
{%- do database.update({'state': 'present'}) %}
{%- endif %}
mysql_database_present_{{ name }}:
# DATABASE PRESENT
{% if database['state'] == "present" %}
  mysql_database.present:
    - name : {{ name }}
    - character_set:  '{{ database['char'] }}'
    - connection_host: localhost
    - connection_pass: {{ percona_settings.get('root_password', '') }}
{% endif %}
# DATABASE ABSENT
{% if database['state'] == "absent" %}
  mysql_database.absent:
    - name : {{ name }}
    - connection_host: localhost
    - connection_pass: {{ percona_settings.get('root_password', '') }}
{% endif %}
{% endfor %}
{% endif %}

mysql_service_running:
  service.running:
    - name: mysql
