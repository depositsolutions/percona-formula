{% from "percona/map.jinja" import percona_settings with context %}
{% set os_family = salt['grains.get']('os_family') %}
{% set initsystem = salt['grains.get']('init') %}
{% set repolist = [] %}
{% if 'repos' in percona_settings and percona_settings.repos is list %}
{%   for repo in percona_settings.repos if repo is mapping and 'name' in repo %}
{% do repolist.append("perconarepo_" + loop.index|string) %}
{%   endfor %}
{% endif %}

include:
  - .repo
  - .client
  - .config
  - .service
  - .motd
  - .deps
  - .users

{% if percona_settings.get('root_password', False) %}
{% if os_family == 'Debian' %}
percona_debconf_utils:
  pkg.installed:
    - name: {{ percona_settings.debconf_utils }}

mysql_debconf:
  debconf.set:
    - name: {{ percona_settings.server_pkg }}-{{ percona_settings.versionstring }}
    - data:
        '{{ percona_settings.server_pkg }}-{{ percona_settings.versionstring }}/root-pass': {'type': 'password', 'value': '{{ percona_settings.root_password }}'}
        '{{ percona_settings.server_pkg }}-{{ percona_settings.versionstring }}/re-root-pass': {'type': 'password', 'value': '{{ percona_settings.root_password }}'}
        '{{ percona_settings.server_pkg }}-{{ percona_settings.versionstring }}/start_on_boot': {'type': 'boolean', 'value': 'true'}
    - require_in:
      - pkg: percona_server
    - require:
      - pkg: percona_debconf_utils
{% elif os_family in ['RedHat', 'Suse'] %}
mysql_root_password:
  mysql_user.present:
    - name: root
    - host: localhost
    - password: {{ percona_settings.root_password }}
    - connection_pass: {{ percona_settings.get('old_root_password', '') }}
    - require:
      - service: percona_svc
      - pkg: mysql_python_dep
{% endif %}
{% endif %}

percona_server_user:
  user.present:
    - name: {{ percona_settings.user }}
    - fullname: 'MySQL Server'
    - shell: /bin/false
    - system: True
    - home: /var/lib/mysql
    - createhome: False
    - optional_groups:
      - ssl-cert

percona_server_group:
  group.present:
    - name: {{ percona_settings.group }}
    - members:
      - {{ percona_settings.user }}
    - require:
      - user: {{ percona_settings.user }}

{% if os_family == 'Debian' %}
percona-server-debian-fix-dir:
  file.directory:
    - name: /etc/mysql/percona-server.conf.d
    - makedirs: True
    - mode: 0755
    - require_in:
      - pkg: percona_server

percona-server-debian-fix-mysqld:
  file.managed:
    - name: /etc/mysql/percona-server.conf.d/mysqld.cnf
    - source: salt://percona/files/debian_percona_mysqld.cnf.j2
    - user: root
    - group: root
    - mode: 0644
    - template: jinja
    - watch_in:
      - service: percona_svc
    - require_in:
      - pkg: percona_server
    - require:
      - file: percona-server-debian-fix-dir
      - user: {{ percona_settings.user }}
      - group: {{ percona_settings.group }}

percona-server-mariadb-audit:
  file.managed:
    - name: /usr/lib/mysql/plugin/server_audit.so
    - source: salt://percona/files/server_audit.so
    - user: root
    - group: root
    - mode: 0644
    - watch_in:
      - service: percona_svc


percona-server-plugin-dir:
  file.directory:
    - name: /usr/lib/mysql/plugin
    - makedirs: True
    - mode: 0755
    - require_in:
      - pkg: percona_server
    - require_in:
      - pkg: percona_server

percona-server-mcafee-audit:
  file.managed:
    - name: /usr/lib/mysql/plugin/libaudit_plugin.so
    - source: salt://percona/files/libaudit_plugin.so
    - user: root
    - group: root
    - mode: 0644
    - watch_in:
      - service: percona_svc
    - require_in:
      - pkg: percona_server
{% endif %}

percona_server:
  pkg.installed:
    - name: {{ percona_settings.server_pkg }}-{{ percona_settings.versionstring }}
    - require:
{% for r in repolist %}
      - pkgrepo: {{ r }}
{% endfor %}

{% if os_family in ['RedHat', 'Suse'] and percona_settings.version >= 5.7 %}
# Initialize mysql database with --initialize-insecure option before starting service so we don't get locked out.
mysql_initialize:
  cmd.run:
    - name: mysqld --initialize-insecure --user=mysql --basedir=/usr --datadir={{ percona_settings.datadir }}
    - user: root
    - creates: {{ percona_settings.datadir }}/mysql/
    - require:
      - pkg: percona_server
    - require_in:
      - service: percona_svc
{% endif %}

{% if initsystem == 'systemd' %}
percona_remove_limits:
  file.managed:
    - name: /etc/systemd/system/mysql.service.d/override.conf
    - makedirs: True
    - user: root
    - group: root
    - mode: 644
    - contents: |
        [Service]
        LimitNOFILE=infinity
        LimitMEMLOCK=infinity
    - require_in:
      - pkg: percona_server
  module.run:
    - name: service.systemctl_reload
    - onchanges:
      - file: /etc/systemd/system/mysql.service.d/override.conf
{%   if percona_settings.reload_on_change %}
    - watch_in:
      - service: percona_svc
{%   endif %}
{% endif %}
